CAROS

[TOC]

# Adding package to CAROS #

A package should contain the following files in the package root directory
 
 - package.xml
 - rosdoc.yaml
 - mainpage.md
 - CMakeLists.txt

# Recreation of the CAROS project #
Due to some issue(s) that made it impossible to do merge requests through GitLab, then this GitLab CAROS project has been recreated.
Unfortunately it didn't automatically accept the new CAROS project as similar to the original, meaning you will need to remove your old fork and create a new fork.
