#ifndef CAROS_CONTROL_GRIPPER_SERVICE_INTERFACE_H
#define CAROS_CONTROL_GRIPPER_SERVICE_INTERFACE_H

#include <rw/math/Q.hpp>

#include <ros/ros.h>

#include <caros_control_msgs/gripper_move_q.h>
#include <caros_control_msgs/gripper_grip_q.h>
#include <caros_control_msgs/gripper_set_force_q.h>
#include <caros_control_msgs/gripper_set_velocity_q.h>
#include <caros_control_msgs/gripper_stop_movement.h>

#include <string>

/* Always publish the latest gripper state */
#define GRIPPER_STATE_PUBLISHER_QUEUE_SIZE 1
#define GRIPPER_SERVICE_INTERFACE_SUB_NAMESPACE "caros_gripper_service_interface"

namespace caros
{
/**
 * @brief This is the gripper interface. It defines the
 * minimum interface that a configuration based robotic grasping device needs
 * to implement.
 *
 * In ROS the namespace of the node is used and it is important that
 * not two GripperServiceInterfaces are running in the same namespace.
 *
 */
class GripperServiceInterface
{
 public:
  /**
   * @brief constructor.
   * @param[in] nodehandle the nodehandle to use for services.
   */
  GripperServiceInterface(const ros::NodeHandle& nodehandle);

  /**
   * @brief virtual destructor
   */
  virtual ~GripperServiceInterface();

  /* FIXME: apidoc documentation */
  bool configureInterface();

  /**
   * @brief signal the gripper to move into a specific configuration Q.
   * @param q
   * @return
   */
  virtual bool moveQ(const rw::math::Q& q) = 0;

  /**
   * @brief signal the gripper to move into a specific configuration Q. The gripper will not show an error in its state
   * if the configuration Q can not be reached.
   * @param q
   * @return
   */
  virtual bool gripQ(const rw::math::Q& q) = 0;

  /**
   * @brief set the desired force configuration that the gripper should use.
   * @param q
   * @return
   */
  virtual bool setForceQ(const rw::math::Q& q) = 0;

  /**
   * @brief set the desired velocity configuration that the gripper should use.
   * @param q
   * @return
   */
  virtual bool setVelocityQ(const rw::math::Q& q) = 0;

  /**
   * @brief signal the gripper to stop all its movements.
   * It should not power down the gripper and/or disconnect from the gripper.
   * @return
   */
  virtual bool stopMovement(void) = 0;

 protected:
  /**
   * @brief publish the state of the gripper. Uses GripperState messages
   * @param[in] q joint configuration
   * @param[in] dq joint velocity
   * @param[in] jointforce joint force
   * @param[in] isMoving is moving
   * @param[in] isBlocked is blocked
   * @param[in] isStopped is stopped
   * @param[in] isEstopped is emergency stopped
   */
  void publishState(const rw::math::Q& q, const rw::math::Q& dq, const rw::math::Q& jointforce, bool isMoving,
                    bool isBlocked, bool isStopped, bool isEstopped);

 private:
  /**
   * @brief private default constructor.
   * This is declared as private to enforce deriving classes to call an available public constructor and enforce that
   * the ROS services are properly initialised.
   */
  GripperServiceInterface();

  /**
   * @brief internal GripperServiceInterface function to properly advertise the services using ROS and setup the
   * callbacks.
   * This should not be called directly.
   */
  bool initService();

  /* - these functions should be grouped together in the documentation t shortly describe that these are converting from
   * ROS types to e.g. RobWork types according to what the interface expects - */
  bool moveQHandle(caros_control_msgs::gripper_move_q::Request& request,
                   caros_control_msgs::gripper_move_q::Response& response);

  bool gripQHandle(caros_control_msgs::gripper_grip_q::Request& request,
                   caros_control_msgs::gripper_grip_q::Response& response);

  bool setForceQHandle(caros_control_msgs::gripper_set_force_q::Request& request,
                       caros_control_msgs::gripper_set_force_q::Response& response);

  bool setVelocityQHandle(caros_control_msgs::gripper_set_velocity_q::Request& request,
                          caros_control_msgs::gripper_set_velocity_q::Response& response);

  bool stopMovementHandle(caros_control_msgs::gripper_stop_movement::Request& request,
                          caros_control_msgs::gripper_stop_movement::Response& response);

 private:
  ros::NodeHandle nodeHandle_;

  ros::Publisher gripperStatePublisher_;

  ros::ServiceServer srvMoveQ_;
  ros::ServiceServer srvGripQ_;
  ros::ServiceServer srvSetForceQ_;
  ros::ServiceServer srvSetVelocityQ_;
  ros::ServiceServer srvStopMovement_;

  /************************************************************************
   * Notes:
   * If it is required to know whether this object is configured/initialised then a variable/state needs to be
   *implemented. Furthermore extra care should be taken to make sure that this object is not left in an "undefined"
   *state. This would happen when some of the ros services or publishers fails to be initialised or shutdown properly.
   ************************************************************************/
};
}  // namespace
#endif
